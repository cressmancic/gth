""" Default urlconf for gth """

from django.conf.urls import include, patterns, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.templatetags.static import static
from django.views.generic.base import RedirectView

admin.autodiscover()


def bad(request):
    """ Simulates a server error """
    1 / 0

urlpatterns = staticfiles_urlpatterns() + patterns('',
    # Examples:
    # url(r'^$', 'gth.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^bad/$', bad),
    # handle favicon (when browser asks for default /favicon.ico)
    url(r'favicon.ico$', RedirectView.as_view(url=static("favicon.ico"))),
    # Main default handler
    url(r'', include('base.urls')),
)

