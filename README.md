# The Goldtoken Enhancements Project #

## DEPRECATION NOTICE ##

This project is being supplanted by a [new version](https://bitbucket.org/cressmancic/gte) that is implemented as a firefox add-on in order to safe-guard and ensure your privacy. There is no 3rd party server through which your account details could be harvested. There is only your browser and the goldtoken server.

## About ##

This project forwards requests on your behalf to the games site goldtoken.com,
adding enhancements to the user interface before sending the result back to your browser.
I have created it for my personal use, but invite any other
players on the site to use it if they wish.
I also invite goldtoken's developers to borrow anything I have done
and incorporate it in their code base (this is why I have made this repository public).

In order to access the service, please click on the link in the "Website" field of the bitbucket overview page
(This should be the page you are currently on).

## DISCLAIMER / TERMS OF USE ##

This service is provided on an AS IS basis.

I do not gaurantee the security of your Goldtoken account.
Your credentials are not stored - however a cookie is used
to maintain access as you play. I, or anyone who is able to
comprimise the server can gain access to your account.
I will not. I believe the server is secure. I trust myself
enough that I will use my own service with my own GT account.
But beyond that, you will use this service at your own risk.

I do not gaurantee that the enhancements I have made will actually work.
After clicking on "end this move" you should review carefully to make sure
that you are submitting the move you think you are.

I do not gaurantee that the service will be available or functional at any particular time.
I plan on making numerous updates and the site will be down while I do so.

By using this service you agree to these terms.

## Enhancements ##

### Xiphias (and variants) ###

Instead of clicking to build an alien and then click to move it and then click the spot to move it to,
just click on the spot adjacent to a planet you own that you wish to move to and a script will
determine how to build or move existing aliens to get there.

In the top row of the gameboard, the controls have been added for force fields, etc.
Click on the symbol to enter into alternative build modes.
For example, click the force field icon to enter forcefield building mode.
Then click one or more spots to build a force field.
Finally, click the force field icon to return to normal build mode.

Xiphiasyn will not work, but I hope to add support eventually.