from collections import defaultdict
import re
import string
from django.conf import settings

BASE_URL = settings.GT_BASE_URL

xiphias_golderos_re = re.compile('(\d+) Golderos \(([+-]\d+)\)')
xiphias_startswith_re = re.compile('^xiphias')
xiphias_alien_re = re.compile('a([sml])([1234f])S.gif$')
alien_power_map = dict(s=1, m=2, l=3)
TD_STYLE = "height: 24px; width: 20px"

def get_xiphias_board(soup):
    """
    Given raw GoldToken xiphias soup, get the gameboard part of it,
    and annotate with various data attributes and ids to allow the script to function.
    """
    # my_elements is currently not used - might come back - see below
    # my_elements = {}

    # Get the table containing the board elements
    gameboard = soup.find("div", id="gameboard")
    if not gameboard:
        # This would happen when selecting color at the beginning, and possibly a couple of other edge cases
        # regardless, if the div does not exist, then there is nothing to do here.
        return None

    # Get the table element that is the root of the planet grid
    xtable = gameboard.find("table", id=xiphias_startswith_re)

    # initialization for loop variables
    my_color = None
    coord_sequence = string.digits[1:] + string.ascii_uppercase

    # Loop through all rows and columns of the table to annotate each planet (<td>) appropriately
    # using the xiphias coordinate system
    # Each <td> element has a rowspan of 2 and the grid staggers so that each coordinate system row is two <tr>s
    # y will be the <tr> index within the <tbody>
    # x will be the <td> index within the <tr> (adjusted for the staggered rowspans - so incrementing by two)
    y = 0
    for tr in xtable.find_all("tr"):
        # initial value of x depens on whether the first <td> in the row is in the first column
        # or in the second column (in which case the first column is inhabited by the rowspanning of the <td> above
        x = (y + 1) % 2
        for td in tr.find_all("td", rowspan=True):
            td_class = td.get('class', '')[0]
            # expect the first character of the class to identify the type of planet
            # - owned by player (p), rocket (r), etc.

            # adjust for viruses - begins with v1 or v2 - the numerical part is irrelevant for our purposes,
            # so strip down to just a "v" prefix.
            if td_class[0] == 'v':
                td_class = td_class[0] + td_class[2:]

            # Unavailable game board hexes have a class that is just "p" - only analyze planet hexes
            if len(td_class) > 1:
                # Squish each <tr> index into groups of two to calculate the grid coordinate
                coord = coord_sequence[x] + coord_sequence[y / 2]
                # identify each hex by its coordinate
                td["id"] = coord

                # Unoccupied planets have a class of "pn" - only further analyze occupied planets.
                if td_class[1] != 'n':
                    # The second character in the class always identifies the player color
                    # - add this as an attribute for convenience.
                    td["data-owner"] = td_class[1]

                    # Iff planet is owned by current player there will be an element (<img> or <div>)
                    # contained in the <td> that records area finances in the "title" attribute.
                    gold_el = td.find(title=xiphias_golderos_re)
                    # annotate finances as data attribute
                    if gold_el:
                        gold_match = xiphias_golderos_re.match(gold_el['title']).groups()
                        td["data-gold"] = int(gold_match[0])
                        td["data-dgold"] = int(gold_match[1])
                        # gold will only be present for current player - record it for future use
                        my_color = td_class[1]

                    # # Remove this? - It is related to region determination method below - see if that still gets used
                    # if td_class[1] != 'n':
                    #     # owned by someone
                    #     grid = (x, y / 2)
                    #     my_elements[grid] = td

                    # Identify if planet is occupied by an alien and add further attributes to identify
                    # strength and whether the alien can be moved.
                    td_img = td.find("img", src=xiphias_alien_re)
                    if td_img:
                        alien_match = xiphias_alien_re.search(td_img["src"]).groups()
                        td["data-alien"] = alien_power_map[alien_match[0]]
                        if alien_match[1] == 'f':
                            td["data-moveable"] = 1
                            # remove a tags from these elements so they do not interfere with custom behaviour
                            td.find("a").replaceWithChildren()
            x += 2
        y += 1

    # Reconfigure the control panel by bringing it inside the game board
    # - requires shorter mouse moves to get to it
    # - allows us to use goldtoken's style sheets to show controls as icons rather than text
    control_panel = soup.find("div", id="short")
    end_link = control_panel.find("a", href=re.compile(";m=(\w)*x"))
    if end_link:
        # Give the "End this move" link an id so that event handler can be attached.
        end_link["id"] = "endMove"
        new_control_panel = soup.new_tag("tr")
        new_control_panel["style"] = "border: solid 1px white; padding: 2px;"
        end_link = end_link.wrap(soup.new_tag("td", colspan=12))
        new_control_panel.append(end_link)

        # add forcefield button
        forcefield_icon = soup.new_tag("td")
        forcefield_icon["name"] = "ff"
        forcefield_icon["class"] = "f%s mode-toggle" % my_color
        forcefield_icon["style"] = TD_STYLE
        forcefield_icon["title"] = "Build force fields"
        new_control_panel.append(forcefield_icon)

        # add bomb button
        # TODO: make this work
        # bomb_icon = soup.new_tag("img", src="%simages/games/xiphias/bbS.gif" % BASE_URL)
        # bomb_icon = bomb_icon.wrap(soup.new_tag("td"))
        # bomb_icon["class"] = "p%s" % my_color
        # bomb_icon["style"] = "height: 24px; width: 20px"
        # new_control_panel.append(bomb_icon)

        # add rocket icon to represent a missile launching action
        missile_icon = soup.new_tag("td")
        missile_icon["name"] = "yy"
        missile_icon["class"] = "r%s mode-toggle" % my_color
        missile_icon["style"] = TD_STYLE
        missile_icon["title"] = "Launch Missile"
        new_control_panel.append(missile_icon)

        # insert the controls into the game board table for more direct (less scrolling) access
        xtable.insert(0, new_control_panel)
        control_panel.decompose()

    # Python version of the region determination now done in the js.
    return xtable

def calculate_regions():
    """
    Currently defunct method to annotate the regions of a board.
    It is now implemented in js instead.
    This might be useful for xiphiasyn ajax calls .
    will need to define my_elements if this is to be used again.
    """
    region_id_seq = 1
    grid_regions = {}
    region_grids = defaultdict(list)
    last_grid = (-99, -99)
    # Go through all planets traversing each column from left to right (achieved by the sort)
    all_grids = my_elements.keys()
    all_grids.sort()
    for grid in all_grids:
        x, y = grid
        dy = (x + 1) % 2
        region = None
        owner = my_elements[grid]["data-owner"]
        if last_grid == (x, y - 1) and owner == my_elements[(x, y - 1)]["data-owner"]:
            # Part of same region as planet directly above
            region = grid_regions[grid] = grid_regions[last_grid]
            region_grids[region].append(grid)
        up_left = (x - 1, y + dy - 1)
        if up_left in grid_regions and owner == my_elements[up_left]["data-owner"]:
            # planet to left and up is owned
            if region:
                # it connects to directly above - must be same
                assert region == grid_regions[up_left]
            region = grid_regions[grid] = grid_regions[up_left]
            region_grids[region].append(grid)
        down_left = (x - 1, y + dy)
        if down_left in grid_regions and owner == my_elements[down_left]["data-owner"]:
            # planet to left and down is owned
            if region and region != grid_regions[down_left]:
                # merge regions in favour of down left
                keep_region = grid_regions[grid] = grid_regions[down_left]
                region_grids[keep_region].append(grid)
                # modify region to keep_region
                obsolete_region = region_grids.pop(region)
                for chg_grid in obsolete_region:
                    grid_regions[chg_grid] = keep_region
                    region_grids[keep_region].append(chg_grid)
            else:
                # same regions as down left
                region = grid_regions[grid] = grid_regions[down_left]
                region_grids[region].append(grid)
        if region is None:
            region = grid_regions[grid] = region_id_seq
            region_grids[region].append(grid)
            region_id_seq += 1
        last_grid = grid

# Part of the former view code that determined available moves for a region.
# some bits of this might be useful for xiphiasyn ajax views to determine defensive powers.
# #detect player color
#             color = soup.find(title=xiphias_golderos_re).find_parent("td")['class'][0][1]
#             # get game board part of soup
#             xtable = get_xiphias_board(soup, annotate_regions=color)
#             # Add links for creating and moving a larger alien
#                 # the last three char of the url is the move, so we want to insert more creates before it
#                 base_url = url[:-3]
#                 coord = url[-2:]
#                 origin = xtable.find("td", id=coord)
#                 # record all current options for move
#                 current_moves = [tag.find_parent("td")["id"] for tag in xtable.find_all("a")]
#                 # Find out how many golderos left
#                 gold = int(xiphias_golderos_re.match(origin.contents[0]["title"]).groups()[0])
#                 for i in (1, 2):
#                     gold -= 10
#                     if gold >= 0:
#                         params = dict(base=base_url, coord=coord)
#                         new_url = '%(base)ssa%(coord)sm%(coord)s' % params
#                         base_url = '%(base)ssa%(coord)s' % params
#                         print new_url
#                         new_soup = BeautifulSoup(opener.open(new_url).read())
#                         new_board = get_xiphias_board(new_soup)
#                         for tag in new_board.find_all("a"):
#                             mcoord = tag.find_parent("td")["id"]
#                             if mcoord not in current_moves:
#                                 current_moves.append(mcoord)
#                                 # Add the link to the original soup
#                                 xtable.find("td", id=mcoord).contents[0].replace_with(tag)
