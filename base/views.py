from cookielib import LWPCookieJar
import datetime
import os
import urllib
import urllib2
import urlparse
from bs4 import BeautifulSoup
from django.conf import settings
from django.http.response import HttpResponse
from django.templatetags.static import static
from django.views.decorators.csrf import csrf_exempt
from base.xiphias import get_xiphias_board

BASE_URL = settings.GT_BASE_URL

@csrf_exempt
def passthrough(request):
    """
    Pass on request to goldtoken, but then add enhancements.
    """
    # Store goldtoken cookies on this server and link each to a unique local session.
    cookiefile = request.session.get('GTH_COOKIEFILE', None)
    if not (cookiefile and os.path.exists(cookiefile)):
        now = datetime.datetime.now()
        cookiedir = getattr(settings, "GT_COOKIE_PATH", "/tmp/gth")
        # ensure specified directory exists
        if not os.path.exists(cookiedir):
            os.makedirs(cookiedir)
        # create cookie file name and set it in the session
        request.session['GTH_COOKIEFILE'] = cookiefile = os.path.join(
            cookiedir,
            # Use full date including microseconds - to ensure uniqueness
            '%s%d' % (now.strftime('%Y%m%d%H%M%S'), now.microsecond)
        )

    # Do not worry if can not load ... LWPCookieJar will create the file at the end if it does not already exist
    cj = LWPCookieJar(filename=cookiefile)
    try:
        cj.load()
    except IOError:
        pass

    # Build the opener that will handle the cookies
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    open_args = []

    # Pass along the POST params if applicable
    if request.method == "POST":
        open_args.append(urllib.urlencode(request.POST))
    url = urlparse.urljoin(BASE_URL, request.path)

    # Allow for a special get param called enhancement that is not to be passed through,
    # but is to be handled as a special case for some kind of enhancement
    enhancement = request.GET.get('enhancement', None)
    qs = request.META['QUERY_STRING']

    # strip enhancement part off of Query string
    # requires that it be positionally at the end of the URL
    # TODO: should not require - can split the GET params and rebuild after popping this one
    try:
        qs = qs[:qs.index('&enhancement')]
    except ValueError:
        pass
    if qs:
        url = '%s?%s' % (url, qs)

    # Retrieve the un-enhanced content from GoldToken
    content = opener.open(url, *open_args).read()
    soup = BeautifulSoup(content)

    ##### Make relative urls absolute #####

    # First, modify the src attribute
    # Apparently they use an input tag with a src attribute on the login page
    for tag in soup.find_all(("input", "img", "script"), src=True):
        tag['src'] = urlparse.urljoin(BASE_URL, tag['src'])

    # Second, modify the href attribute
    # Not for <a> tags because we want them to remain relative and pass through the enhancements server
    for tag in soup.find_all("link"):
        tag['href'] = urlparse.urljoin(BASE_URL, tag['href'])

    # Third, modify the background attribute if it is a path (starts with /)
    # goldtoken sometimes gives background attributes to td/table elements that are image paths
    for image in soup.find_all(("td", "th", "table")):
        if image.has_attr('background') and image['background'].startswith('/'):
            image['background'] = urlparse.urljoin(BASE_URL, image['background'])

    ##### Enhancements #####
    # Add the local favicon link
    soup.head.append(soup.new_tag("link", rel="shortcut icon", type="image/ico", href=static("favicon.ico")))

    # Add jquery
    soup.head.append(soup.new_tag("script", src=static("js/libs/jquery-2.1.1.min.js")))

    # Move "last move" info below the board
    # (This should allow uniformity of board positioning when navigating past moves)
    lastmove = soup.find("div", id="lastmove")
    if lastmove:
        soup.find("div", id="pastmoves").insert(0, lastmove)

    # determine game type and add scripts accordingly
    try:
        game_type = soup.title.text.split(":")[0]
    except IndexError:
        game_type = None

    if game_type == u'Grabble':
        soup.head.append(soup.new_tag("script", src=static("js/grabble.js")))

    if game_type.startswith(u'Xiphias'):
        # add data attributes to xiphias board that script will be expecting
        x = get_xiphias_board(soup)
        # add the xiphias script if the gameboard exists
        if x is not None:
            soup.head.append(soup.new_tag("script", src=static("js/xiphias.js")))

    # save the cookie
    cj.save()

    # return the enhanced response
    return HttpResponse(str(soup))
