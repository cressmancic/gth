var coordSequence = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var imagePath = "http://goldtoken.com/images/games/xiphias/";
// image file needed to display aliens indexed by power level
// - power level may be zero (no alien) in which case return false
var alienImg = [false, "as1S.gif", "am1S.gif", "al1S.gif"];
// gold consumption of aliens indexed by power level
var alienConsume = [0, 2, 6, 20];
// store the full move globally
var cummulativeMove = "";
// global counter for assigning region ids
var region_id_seq = 1;
// mode to be controlled by the various build options in the control panel
var buildMode = "normal";
// global to remember a part of an action that needs a further step before completing
var tempAction = "";
var tempRegion = "";

function addMessage(msg) {
    console.log(msg);
}

function escapeSpecialBuildMode() {
    buildMode = "normal";
    $('.mode-toggle').css('background-color', 'black');
}

function getAdj(hex){
    /*
    Given a character string `hex`, search for and return any planets in the six surrounding grid positions.
     */

    // convert to numerical indices
    var x = coordSequence.indexOf(hex[0]);
    var y = coordSequence.indexOf(hex[1]);

    // determine the row offset to use depending on if we are in a row that is
    // relatively half shifted up or half shifted down.
    var dy = (x + 1) % 2;
    var hexes = [];

    // compute possible adjacent indices
    var xSeq = [x, x + 1, x + 1, x, x - 1, x - 1];
    var ySeq = [y - 1, y + dy - 1, y + dy, y + 1, y + dy, y + dy - 1];

    // Loop through and store the actual elements that are valid adjacent planets
    for (var i = 0; i < 6; i++){
        var el = $('#' + coordSequence[xSeq[i]] + coordSequence[ySeq[i]]);
        if (el.length > 0) {
            hexes.push(el);
        }
    }
    return hexes;
}

function calculateRegions(collection) {
    /*
    For all elements in the collection, determine connectivity and annotate with a unique id per connected region.
    Annotate each element with a "region" data attribute.

    Return the number of regions found
     */
    var numRegions = 0;

    // Build mapping of coordinates to elements in the collection
    var grid_elements = {};
    collection.each(function () {
        grid_elements[this.id] = this;
    });

    // Go through all planets traversing each column from left to right (achieved by the sort)
    var grids = Object.keys(grid_elements);
    grids.sort()
    for (var i = 0; i < grids.length; i++) {
        //convert to numerical indices
        var x = coordSequence.indexOf(grids[i][0]);
        var y = coordSequence.indexOf(grids[i][1]);

        // calculate offset (see explanation elsewhere)
        var dy = (x + 1) % 2;

        // initialize region to invalid value so we know if and of the following checks test positive
        var region = 0;
        var el = $('#'+grids[i]);
        var owner = el.attr('data-owner');

        // Check the planet directly up, then up and to the left, then down and to the left
        // Because of the order of traversal, these are the planets that have already been dealt with if
        // they are in the same region.
        // If any of these are of the same owner, then the region must be the same.
        var up = $('#' + coordSequence[x] + coordSequence[y - 1]);
        if (owner == up.attr('data-owner')) {
            // Part of same region as planet directly above
            region = up.attr('data-region');
            // Set this region to the same id
            el.attr('data-region', region);
        }
        var up_left = $('#' + coordSequence[x - 1] + coordSequence[y + dy - 1]);
        if (owner == up_left.attr('data-owner')) {
            // Part of same region as planet to left and up
            region = up_left.attr('data-region');
            // Set this region to the same id
            el.attr('data-region', region);
        }
        var down_left = $('#' + coordSequence[x - 1] + coordSequence[y + dy]);
        if (owner == down_left.attr('data-owner')) {
            // Part of same region as planet to left and down
            var down_left_region = down_left.attr('data-region');
            if (region > 0 && region != down_left_region) {
                // Not part of same region previously assigned
                // ie. the planet directly up is of a different region that the down left planet
                // merge regions in favour of down left
                collection.filter('[data-region=' + region + ']').attr('data-region', down_left_region);
                numRegions --;
            }
            else {
                region = down_left.attr('data-region');
                el.attr('data-region', region);
            }
        }
        if (region == 0) {
            // new region
            el.attr('data-region', region_id_seq);
            region_id_seq ++;
            numRegions ++;
        }
    }
    return numRegions;
}

function placeRocket(region) {
    /*
    Determine of current region needs a rocket and place it in the appropriate location
    This is called on both regions produced by a split, so have the time there will
    already be a rocket and no action to take.
     */
    var tdClass;
    var collection = $('td[data-region=' + region + ']');
    // check for existing rocket
    if (collection.filter('[class^=r]').length > 0) {
        if (collection.length == 1) {
            // clear the rocket from area with one planet
            tdClass = collection.attr('class');
            collection.attr('class', 'p' + tdClass[1]);
        }
    }
    // place new rocket as long as there is more than one planet
    else if (collection.length > 1) {
        var grids = [];
        collection.each(function () {
            // reverse the id so that sort will be by row then by column
            grids.push(this.id[1]+this.id[0]);
        });
        grids.sort();
        // Rocket should be placed in first column of first row (so the first element of this sorted list)
        // re-reverse it
        var el = $('#' + grids[0][1] + grids[0][0]);
        tdClass = el.attr('class');
        // specifically index the last char because a virus class v1c should become rc while a force field fc becomes rc
        el.attr('class', 'r' + tdClass[tdClass.length - 1]);
    }
}

function regionGold(region) {
    /*
    return gold and dgold for a region
     */
    var regionSelector = $('td[data-region=' + region + ']');
    return {
        gold: parseInt(regionSelector.attr('data-gold')),
        dgold: parseInt(regionSelector.attr('data-dgold'))
    };
}

function updateRegionGold(region, gold, dgold) {
    /*
     update region wide attributes
      */
    $('td[data-region=' + region + ']').each(function () {
        $(this).attr('data-gold', gold);
        $(this).attr('data-dgold', dgold);
        $(this).children().first().attr('title', gold + " Golderos (" + dgold + ")");
    })
}

function getDefensivePower(el) {
    /*
    Get the defensive power of a planet.
    Maximum value of either a present alien's power or the defensive power of a rocket or force field.
     */
    // Level of alien
    var alien = $(el).attr('data-alien') || 0;
    // Level of other defense (force field, rocket)
    var cl = $(el).attr('class');
    var other = 0;
    if (cl.indexOf('f') == 0) {
        other = 2;
    }
    else if (cl.indexOf('r') == 0) {
        other = 1;
    }
    return Math.max(alien, other);
}

function gatherDrones(number, drones, gold, createZone) {
    /*
    return the partial action that will move requested number of drones (either existing or created)
    with the "to" not specified
     */
    var gatherPoint, partialAction = '';

    // if using existing drones, we need to gather them to the location of one of them so that the final alien will
    // still be allowed to move.
    if (drones.length > 0) {
        gatherPoint = drones[0];
        number -= 1;
    }
    else {
        // if no existing drones, use the default provided
        gatherPoint = createZone;
    }

    // gather existing drones until we run out or have the required number
    for (var i = 1; i < drones.length && number > 0; i++) {
        partialAction += 'm' + drones[i] + gatherPoint;
        number -= 1;
    }

    // then build new drones until we run out of gold or have the required number
    for (; number > 0 && gold >= 10; number--) {
        partialAction += 'a' + gatherPoint;
        gold -= 10;
    }

    // number should end up at zero if action is possible
    if (number > 0) return false;
    return partialAction + 'm' + gatherPoint;
}

function calculateMove(region, hex, number){
    /*
    Calculate move instructions to move alien(s) from region to hex
    return false if not possible
     */
    var action = '', requiredDrones = 0, gatherPoint;
    var regionSelector = $('td[data-region=' + region + ']');
    // Existing aliens that can be moved have the data-moveable attribute set
    var moveableAliens = regionSelector.filter('[data-moveable]');
    // A suitable location to build new aliens is any planet in the region with no existing aliens and no virus
    var createZone
    try {
        createZone = regionSelector.filter(':not([data-alien],[class^="v"])')[0].id;
    }
    catch (err) {
        createZone = false;
    }
    var gold = parseInt(regionSelector.attr('data-gold'));

    // Build up inventory of existing aliens according to their levels
    var moveableLevels = [[],[],[]];
    for (var i = 0; i < moveableAliens.length; i++){
        moveableLevels[moveableAliens[i].dataset.alien - 1].push(moveableAliens[i].id);
    }

    switch (number) {
        case 3:
            if (moveableLevels[2].length > 0) {
                // Move existing captain
                return 'm' + moveableLevels[2][0] + hex;
            }
            else if (moveableLevels[1].length > 0) {
                // Gather 1 drone and move the available soldier on top of it as a "prefixed move"
                action = gatherDrones(1, moveableLevels[0], gold, createZone);
                if (action != false) {
                    gatherPoint = action.slice(-2);
                    return 'm' + moveableLevels[1][0] + gatherPoint + action + hex;
                }
                else {
                    return false;
                }
            }
            else {
                // try to gather 3 drones together
                action = gatherDrones(3, moveableLevels[0], gold, createZone);
                return action ? action + hex : false;
            }
        case 2:
            if (moveableLevels[1].length > 0) {
                // Move existing Soldier
                return 'm' + moveableLevels[1][0] + hex;
            }
            else {
                // Try to gather 2 drones
                action = gatherDrones(2, moveableLevels[0], gold, createZone);
                if (action != false){
                    return action + hex;
                }
                else {
                    // Fallback to moving a Captain if this is the only option left
                    if (moveableLevels[2].length > 0) {
                        return 'm' + moveableLevels[2][0] + hex;
                    }
                    return false;
                }
            }
        case 1:
            // Try to use existing drone
            action = gatherDrones(1, moveableLevels[0], gold, createZone);
            if (action != false) {
                return action + hex;
            }
            // fallback to moving a Soldier, then Captain
            else if (moveableLevels[1].length > 0) {
                return 'm' + moveableLevels[1][0] + hex;
            }
            else if (moveableLevels[2].length > 0) {
                return 'm' + moveableLevels[2][0] + hex;
            }
            else {
                return false;
            }
    }
    return false;
}

function commitMove(action, region){
    /*
    parse the action and update the DOM to reflect it
     */
    var moveType;
    var regionSelector = $('td[data-region=' + region + ']');
    var gold = parseInt(regionSelector.attr('data-gold'));
    var dgold = parseInt(regionSelector.attr('data-dgold'));
    var owner = regionSelector.attr('data-owner');
    var hex1, hex2, alienLevel, alienLevel2, previousOwner, tdClass;
    var n, r, splitRegions, adjHexes, previousRegion, calcRocket;
    while (action != "") {
        moveType = action[0];
        // Add an alien
        if (moveType == 'a') {
            // Reduce gold by 10
            gold -= 10;
            // Get location for creation
            hex1 = $('#' + action.substr(1,2));
            // Get existing alien level
            alienLevel = parseInt(hex1.attr('data-alien')) || 0;
            // Adjust gold consumption for the increased alien level
            dgold = dgold + alienConsume[alienLevel] - alienConsume[alienLevel + 1];
            alienLevel += 1;
            // Add the alien image to the DOM
            hex1.html('<img src="' + imagePath + alienImg[alienLevel] + '"></img>');
            // Update the data attributes to reflect the new alien
            hex1.attr('data-alien', alienLevel);
            if (alienLevel < 2) {
                // Should become moveable if it is a drone
                // otherwise it should retain the moveability of the alien that was already there
                hex1.attr('data-moveable', 1);
            }
            action = action.substr(3);
        }
        // Launch missile
        else if (moveType == 'm' && action[3] == 'y') {
            hex1 = $('#' + action.substr(1, 2));
            hex2 = $('#' + action.substr(5, 2));
            // mark and show launching alien as unmoveable
            alienLevel = parseInt(hex1.attr('data-alien'));
            hex1.removeAttr('data-moveable');
            hex1.html('<img src="' + imagePath + alienImg[alienLevel] + '"></img>');
            // consume gold
            gold -= 100;
            // destroy target
            hex2.html('<div></div>');
            var targetClass = hex2.attr('class');
            if (targetClass[0] == 'p') {
                hex2.attr('class', 'm' + targetClass.substr(1));
            }
            hex2.removeAttr('data-alien');
            // finally, return to normal build mode
            escapeSpecialBuildMode();
            action = action.substr(7);
        }
        // Move an alien
        else if (moveType == 'm') {
            hex1 = $('#' + action.substr(1, 2));
            hex2 = $('#' + action.substr(3, 2));
            // As a special case, if the region initiating the move has only one planet,
            // then that planet gets a rocket as it expands to two planets.
            if (regionSelector.length == 1) {
                hex1.attr('class', 'r' + owner);
                dgold -= 2;
            }
            // Get alien level being moved
            alienLevel = parseInt(hex1.attr('data-alien'));
            dgold += alienConsume[alienLevel];
            hex1.removeAttr('data-alien');
            hex1.removeAttr('data-moveable');
            hex1.html('<div></div>');
            // If hex2 is same owner, then combine alien levels.
            previousOwner = hex2.attr('data-owner');
            alienLevel2 = previousOwner == owner ? parseInt(hex2.attr('data-alien')) || 0 : 0;
            // store previous region and if rocket needs to be moved to be reconciled after planet is taken over.
            previousRegion = hex2.attr('data-region');
            calcRocket = hex2.attr('class')[0] == 'r';
            // Increment alien level
            alienLevel += alienLevel2;
            // Calculate new finances
            dgold = dgold + alienConsume[alienLevel2] - alienConsume[alienLevel];
            // Set appropriate image/data in hex2
            hex2.html('<img src="' + imagePath + alienImg[alienLevel] + '"></img>');
            hex2.attr('data-alien', alienLevel);
            // update owner - or if virus, similar updates required
            tdClass = hex2.attr('class');
            if (previousOwner != owner || tdClass[0] == 'v') {
                hex2.attr('data-region', region);
                hex2.attr('data-owner', owner);
                // force fields stay, but anything else gets the class overwritten
                if (tdClass[0] == 'f') {
                    tdClass = 'f' + owner;
                }
                else {
                    tdClass = 'p' + owner;
                }
                hex2.attr('class', tdClass);
                dgold += 1;
            }
            if (previousOwner != owner) {
                // recalculate regions for affected region
                n = calculateRegions($('td[data-region=' + previousRegion + ']'));
                if (n > 1 || calcRocket) {
                    // there is a split or a rocket was captured and we need to find the new region and place a new one.
                    splitRegions = [];
                    adjHexes = getAdj(hex2.attr('id'));
                    for (var i = 0; i < adjHexes.length; i++) {
                        r = adjHexes[i].attr('data-region');
                        if (adjHexes[i].attr('data-owner') == previousOwner && splitRegions.indexOf(r) == -1) {
                            splitRegions.push(r);
                        }
                    }
                    for (var i = 0; i < splitRegions.length; i++){
                        placeRocket(splitRegions[i]);
                    }
                }
            }
            // increment to next part of action
            action = action.substr(5);
        }
        else if (moveType == 'f') {
            gold -= 15;
            hex1 = $('#' + action.substr(1, 2));
            hex1.attr('class', 'f' + owner);
            action = action.substr(3);
        }
        else {
            alert('unknown move type: ' + moveType);
            action = "";
        }
        // update rocket to not flash if gold drops below 10
        if (gold < 10) {
            var rocket = regionSelector.filter('[class^=r]');
            var rocket_class = rocket.attr('class');
            rocket.attr('class', rocket_class.replace("f", ""));
        }
    }
    updateRegionGold(region, gold, dgold);
}

$(document).ready(function() {
    // Determine the current move at page load time in case a partial move has already been saved
    // this would become the base of any calculated move going forward.
    var endMoveLinkBase = $('#endMove').attr('href');
    // strip the "x" off the end
    if (typeof(endMoveLinkBase) != "undefined") {
        endMoveLinkBase = endMoveLinkBase.substring(0, endMoveLinkBase.length - 1);
    }
    var board = $('#gameboard table[id^="xiphias"]');
    // get current player - all current player planets will have gold attribute.
    var player = $('td[data-gold]').attr('data-owner');

    // Annotate regions
    console.log("Found " + calculateRegions($('td[data-owner]')) + " regions.");

    // Set up handler for any click on the game board
    $('#gameboard').on("click", "td", function() {
        // Check if element is a build mode control icon
        if ($(this).hasClass("mode-toggle")) {
            var mode = $(this).attr('name');
            // if clicking same build mode, then we are toggling back to normal
            // use a "starts with" logic so that rocket mode can have a two step (two mode) process
            if (buildMode.indexOf(mode) == 0) {
                escapeSpecialBuildMode();
            }
            else {
                buildMode = mode;
                $(this).css('background-color', 'red');
            }
            return false;
        }

        var tdClass = this.className;
        var coord = this.id;
        // Check if hex is blank
        if (tdClass.length < 2) {
            return;
        }
        // Store the current state of the game board for forward/back navigation
        // TODO: implement
        var action = false, region, adjRegions = [], regions = [], regionOptions = [];

        // Check if already owned
        if ($(this).attr('data-owner') == player) {
            region = $(this).attr('data-region');
            var gold = regionGold(region).gold;
            // If in special build mode, then take action accordingly
            switch (buildMode) {
                case "normal":
                    action = calculateMove(region, coord, 1);
                    break;
                case "ff":
                    if (gold >= 15 && tdClass[0] == 'p') {
                        action = "f" + coord;
                    }
                    break;
                case "yy":
                    // can not launch from virus
                    if (tdClass[0] == 'v') {
                        break;
                    }
                    // otherwise must have moveable alien + 100 or blank space with 110
                    var has_alien = $(this).attr('data-alien') > 0;
                    var is_moveable = $(this).attr('data-moveable') > 0;
                    if (has_alien && is_moveable && gold >= 100){
                        tempAction = "m" + coord;
                    }
                    else if (!has_alien && gold >= 100) {
                        tempAction = "a" + coord + "m" + coord;
                    }
                    else {
                        // either not enough gold or spot occupied
                        break;
                    }
                    // advance mode to part 2
                    buildMode += "2";
                    // remember the region
                    tempRegion = region;
                    addMessage("Now click the target Hex.");
                    return;
            }
        }
        // special case in launching rocket against opponent, we have a completely separate way of handling things
        else if (buildMode == "yy2") {
            var owner = $(this).attr('data-owner');
            var has_alien = $(this).attr('data-alien') > 0;
            if (owner != player && has_alien) {
                action = tempAction + "yy" + coord;
                region = tempRegion;
            }
            else {
                addMessage("Can not target that location.");
                return;
            }
        }
        else {
            // Or, make sure there is an owned region adjacent
            var owner = $(this).attr('data-owner');
            var defense = getDefensivePower(this);
            var adjElements = getAdj(coord)
            for (var i = 0; i < adjElements.length; i++) {
                // if adjacent element is owned by current player then it could be a route of attack
                if ($(adjElements[i]).attr('data-owner') == player) {
                    var adjRegion = $(adjElements[i]).attr('data-region');
                    // take note of region if not already in list
                    if (regions.indexOf(adjRegion) == -1){
                        regions.push(adjRegion);
                    }
                }
                // update defensive power for adjacent planet owned by same opponent
                if (owner == $(adjElements[i]).attr('data-owner')) {
                    // check for defensive power
                    defense = Math.max(defense, getDefensivePower(adjElements[i]));
                }
            }
            if (regions.length == 0 || defense > 2){
                alert("Can not play there");
                return false;
            }

            // loop through adjacent regions to determine if able to take this spot
            for (var i = 0; i < regions.length; i++) {
                region = regions[i]
                action = calculateMove(region, coord, defense + 1);
                if (action) {
                    regionOptions.push({region: region, action: action});
                }
            }

            // Prompt for selection between multiple options in merge scenario.
            if (regionOptions.length > 1){
                var selection = regionOptions[window.prompt("There are multiple areas from which you can take that planet." +
                    "Please enter the number (1, 2 or, if applicable, 3) of the area to use." +
                    "The areas are numbered starting at the planet above and circling around in a clockwise fashion." +
                    "It is areas that you own that are numbered.", "1") - 1];
                if (typeof(selection) == "undefined") {
                    action = false;
                }
                else {
                    action = selection.action;
                    region = selection.region;
                }
            }
            else if (regionOptions.length == 1){
                action = regionOptions[0].action;
                region = regionOptions[0].region;
            }
        }
        // If action is calculated, then commit it
        if (action) {
            commitMove(action, region);
            // Merge regions if applicable
            for (var i = 0; i < regions.length; i++) {
                var mergeRegion = regions[i];
                if (region != mergeRegion){
                    var keepRegionSelector = $('td[data-region=' + region + ']');
                    var mergeRegionSelector = $('td[data-region=' + mergeRegion + ']');
                    var gold = parseInt(keepRegionSelector.attr('data-gold'));
                    gold += parseInt(mergeRegionSelector.attr('data-gold'));
                    var dgold = parseInt(keepRegionSelector.attr('data-dgold'));
                    dgold += parseInt(mergeRegionSelector.attr('data-dgold'));
                    // Remove rocket if applicable
                    if (mergeRegionSelector.filter('[class^=r]').length > 0) {
                        mergeRegionSelector.filter('[class^=r]').attr('class', 'p' + player);
                        dgold += 2;
                    }
                    mergeRegionSelector.attr('data-region', region);
                    updateRegionGold(region, gold, dgold);
                }
            }
            cummulativeMove += action;
            console.log(cummulativeMove);
            // update the end move link
            $('#endMove').attr('href', endMoveLinkBase + cummulativeMove + 'x');
        }
        else {
            alert("Could not perform move");
        }
    });
});
